import { createApp } from 'vue';
import { createStore } from 'vuex';

import App from './App.vue';

// const counterModule = {
//     state() {
        
//     },
//     mutations: {},
//     actions: {},
//     getters: {}
// };

const store = createStore({
    state() {
        return {
            counter: 0,
            isLoggedIn: false
        };
    },
    mutations: {
        increment(state) {
            state.counter = state.counter + 1;
        },
        increase(state, payload) {
            state.counter = state.counter + payload.value;
        },
        setAuth(state, payload) {
            state.isLoggedIn = payload.isAuth;
        }
    },
    actions: {
        increment(context) {
            setTimeout(function() {
                context.commit('increment');
            }, 2000);
        },
        increase(context, payload) {
            console.log(context);
            context.commit('increase', payload);
        },
        login(context) {
            context.commit('setAuth', {isAuth: true});
        },
        logout(context) {
            context.commit('setAuth', {isAuth: false});
        }
    },
    getters: {
        userIsAuth(state) {
            return state.isLoggedIn;
        },
        // finalCounter(state) {
        //     return state.counter * 2;
        // }
        userIsAuthenticated(state) {
            return state.isLoggedIn;
        }
    }
});


const app = createApp(App);

app.use(store);

app.mount('#app');
