const app = Vue.createApp({
    data() {
        return {
            friends: [
            { id: 'manuel', name: 'Manuel Lorenz', phone: '123456789'},
            { id: 'julie', name: 'Julie Jones', phone: '123456789'},
            ],
        };
    }
});

app.component('friend-contact', {
    template: `
    <li>
        <h2>{{ friend.name }}</h2>
        <button @click="toggleDetails()">Show Details</button>
        <ul v-if="detailsAreVisible">
        <li><strong>Phone:</strong>{{ friend.phone }}</li>
        </ul>
    </li>
    `,
    data() {
        return { 
            detailsAreVisible: false,
            friend: {
                id: 'manuel', name: 'Manuel Lorenz', phone: '123456789'
            },
        };
    },
    methods: {
        toggleDetails() {
            this.detailsAreVisible = !this.detailsAreVisible;
        }
    }
});

app.mount('#app');