const app = Vue.createApp({
    data(){
        return {
            boxAselect: false,
            boxBselect: false,
            boxCselect: false
        };
    },
    computed: {
        boxAClasses() {
            return {active: this.boxAselect};
        }
    },
    methods: {
        boxSelected(box) {
            if (box === 'A') {
                this.boxAselect = true;
            }
        }
    }
});

app.mount('#styling')