const app = Vue.createApp({
  data() {
    return {
      counter: 10,
      name: '',
    };
  },
  watch: {
    counter(value) {
      if (value > 50) {
        this.counter = 0;
      }
    }
    // name(value) {
    //   if (value === '') {
    //     this.fullname = '';
    //   } else {
    //   this.fullname = value + ' ' + 'Nuzzo';
    //   }
    // },
  },
  computed: {
    fullname() {
      if (this.name === '') {
        return '';
      }
      return this.name + ' ' + 'Nuzzo';
    }
  },
  methods: {
    outputFullname() {
      if (this.name === '') {
        return '';
      }
      return this.name + ' ' + 'Nuzzo';
    },
    submitForm() {
      alert('submitted');
    },
    setName(event) {
      this.name = event.target.value;
    },
    add(n) {
      this.counter = this.counter + n;
    },
    subtract(n) {
      this.counter = this.counter - n;
    },
    resetInput() {
      this.name = '';
    }
  }
});

app.mount('#events');
